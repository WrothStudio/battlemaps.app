
# Welcome

## Purpose

This forum is a suggestion box, bug report area, and help desk for [battlemaps.app](https://battlemaps.app/).

Anyone is welcome to submit, but you will need to [make a free GitLab account](https://gitlab.com/users/sign_in) to do so.

If you see a bug or suggestion you like, give them a thumbs up or a comment to let us know you confirm the bug or support the suggestion. 

## Submitting Issues

[The Board](https://gitlab.com/WrothStudio/battlemaps.app/-/boards) here works on [Issues](https://gitlab.com/WrothStudio/battlemaps.app/issues). For more details on how to use the system see the [knowlege base](https://docs.gitlab.com/ee/user/project/issues/) for support.

When you **[submit a new issue](https://gitlab.com/WrothStudio/battlemaps.app/issues/new)**, please put one of the following labels in the title so we can sort it on the board nicely.

## Labels

### Suggestion

An important feature missing? Think of a feature that would be helpful? Got a good idea to make the app better? Let us know about it!

### Question

Something unclear or confusing in the app? Ask us, not only will you get answers but it will help make the app better if we know we are not clear enough in specific places.

### Bug

If you are leaving a bug report it's really helpful to get some background information. Most important steps that caused the bug, what happened vs what you expected, and some info on your situation. You can upload screenshots to help if you want, picture worth a thousand words sometimes.

When you are posting an issue there is a *"Description: Choose a template"* popup menu just under the type, if you select 'bug' from this list it will auto-fill in this template:


> **Steps to reproduce the bug:** 
> ...
> 
> **Resulting and expected behaviour:**
>  ...
> 
> **System information:**
> - Operating system:
> - Browser:
> - Device:
> 
> **Screenshot:** 
> ...


